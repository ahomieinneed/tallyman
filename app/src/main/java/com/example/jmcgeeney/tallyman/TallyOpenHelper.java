package com.example.jmcgeeney.tallyman;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by jmcgeeney on 3/12/15.
 */
public class TallyOpenHelper extends SQLiteOpenHelper {

    public static final String TALLY_TABLE_NAME = "tally";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_COUNT = "count";

    private static final String DATABASE_NAME = "tally.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TALLY_TABLE_CREATE =
            "CREATE TABLE " + TALLY_TABLE_NAME + " (" +
                    KEY_ID + " integer primary key autoincrement, " +
                    KEY_NAME + " TEXT NOT NULL, " +
                    KEY_COUNT + " INT);";

    TallyOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TALLY_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TallyOpenHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TALLY_TABLE_NAME);
        onCreate(db);
    }
}
