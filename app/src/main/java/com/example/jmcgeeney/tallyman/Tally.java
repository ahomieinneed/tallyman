package com.example.jmcgeeney.tallyman;

/**
 * Created by jmcgeeney on 3/12/15.
 */

public class Tally {
    private long id;
    private String name;
    private int count;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String comment) {
        this.name = comment;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void increment() {
        count += 1;
    }

    public void decrement() {
        count -= 1;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return name;
    }
}