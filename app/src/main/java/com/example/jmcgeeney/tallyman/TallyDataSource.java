package com.example.jmcgeeney.tallyman;

/**
 * Created by jmcgeeney on 3/13/15.
 */
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import org.w3c.dom.Comment;

public class TallyDataSource {

    // Database fields
    private SQLiteDatabase database;
    private TallyOpenHelper dbHelper;
    private String[] allColumns = {
            TallyOpenHelper.KEY_ID,
            TallyOpenHelper.KEY_NAME,
            TallyOpenHelper.KEY_COUNT
    };

    public TallyDataSource(Context context) {
        dbHelper = new TallyOpenHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public String createTally(Tally tally) {
        ContentValues values = new ContentValues();
        values.put(TallyOpenHelper.KEY_NAME, tally.getName());
        values.put(TallyOpenHelper.KEY_COUNT, 0);
        long insertId = database.insert(TallyOpenHelper.TALLY_TABLE_NAME, null,
                values);
        Cursor cursor = database.query(TallyOpenHelper.TALLY_TABLE_NAME,
                allColumns, TallyOpenHelper.KEY_ID + " = " + insertId, null,
                null, null, null);
        return tally.getName();
    }

    public void deleteTally(Tally tally) {
        long id = tally.getId();
        System.out.println("Tally deleted with id: " + id);
        database.delete(TallyOpenHelper.TALLY_TABLE_NAME, TallyOpenHelper.KEY_ID +
                " = " + id, null);
    }

    public List<Tally> getAllTallies() {
        List<Tally> tallies = new ArrayList<Tally>();

        Cursor cursor = database.query(TallyOpenHelper.TALLY_TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Tally tally = cursorToTally(cursor);
            tallies.add(tally);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return tallies;
    }

    private Tally cursorToTally(Cursor cursor) {
        Tally tally = new Tally();
        tally.setId(cursor.getLong(0));
        tally.setName(cursor.getString(1));
        tally.setCount(cursor.getInt(2));
        return tally;
    }
}
